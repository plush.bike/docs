/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: "Plush.bike Documentation",
  url: "https://your-docusaurus-test-site.com",
  baseUrl: "/",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/favicon.ico",
  themeConfig: {
    navbar: {
      logo: {
        alt: "Plush.bike Documentation",
        src: "img/logo-indigo.svg",
      },
      items: [
        {
          type: "doc",
          docId: "graphql/schema",
          position: "left",
          label: "GraphQL schema",
        },
        {
          type: "doc",
          docId: "architecture/index",
          position: "left",
          label: "Architecture",
        },
        {
          type: "doc",
          docId: "local-development/index",
          position: "left",
          label: "Local development",
        },
        {
          href: "https://gitlab.com/plush.bike",
          label: "GitLab",
          position: "right",
        },
      ],
    },
    footer: {
      style: "dark",
      links: [],
      copyright: `Copyright © ${new Date().getFullYear()} Plush.bike`,
    },
  },
  presets: [
    [
      "@docusaurus/preset-classic",
      {
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      },
    ],
  ],
  plugins: [
    [
      "@edno/docusaurus2-graphql-doc-generator",
      {
        loaders: {
          UrlLoader: "@graphql-tools/url-loader",
        },
      },
    ],
  ],
};

import React from "react";
import styles from "./HomepageFeatures.module.css";
import Link from "@docusaurus/Link";

const FeatureList = [
  {
    title: "GraphQL schema",
    Svg: require("../../static/img/graphql-logo.svg").default,
    description: <>Browse the Plush.bike GraphQL schema</>,
    to: "/docs/graphql",
    buttonLabel: "Browse GraphQL schema",
  },
  {
    title: "Architecture",
    imgSrc: require("../../static/img/architecture-icon.png").default,
    description: <>Learn more about Plush.bike architecture</>,
    to: "/docs/architecture",
    buttonLabel: "Learn more",
  },
  {
    title: "Local development",
    Svg: require("../../static/img/local-development-icon.svg").default,
    description: <>Learn how to work on Plush.bike locally</>,
    to: "/docs/local-development",
    buttonLabel: "Learn more",
  },
];

function Feature({ Svg, imgSrc, title, description, to, buttonLabel }) {
  return (
    <div className="col col--4 margin-top--lg">
      <div className="text--center">
        {Svg ? (
          <Svg className={styles.featureSvg} alt={title} />
        ) : (
          <img src={imgSrc} alt={title} />
        )}
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
        <Link className="button button--primary button--outline" to={to}>
          {buttonLabel}
        </Link>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
